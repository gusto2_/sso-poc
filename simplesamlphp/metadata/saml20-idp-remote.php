<?php

/**
 * SAML 2.0 remote IdP metadata for SimpleSAMLphp.
 *
 * Remember to remove the IdPs you don't use from this file.
 *
 * See: https://simplesamlphp.org/docs/stable/simplesamlphp-reference-idp-remote
 */

$metadata['http://ssoidp.sse-consulting.eu/simplesaml/saml2/idp/metadata.php'] = array (
  'metadata-set' => 'saml20-idp-remote',
  'entityid' => 'http://ssoidp.sse-consulting.eu/simplesaml/saml2/idp/metadata.php',
  'SingleSignOnService' => 
  array (
    0 => 
    array (
      'Binding' => 'urn:oasis:names:tc:SAML:2.0:bindings:HTTP-Redirect',
      'Location' => 'http://ssoidp.sse-consulting.eu/simplesaml/saml2/idp/SSOService.php',
    ),
  ),
  'SingleLogoutService' => 
  array (
    0 => 
    array (
      'Binding' => 'urn:oasis:names:tc:SAML:2.0:bindings:HTTP-Redirect',
      'Location' => 'http://ssoidp.sse-consulting.eu/simplesaml/saml2/idp/SingleLogoutService.php',
    ),
  ),
  'certData' => 'MIIDXTCCAkWgAwIBAgIJAJvBsHwtnEZRMA0GCSqGSIb3DQEBCwUAMEUxFTATBgNVBAMMDFNTTyBJZFAgVGVzdDENMAsGA1UECwwEVEVTVDEQMA4GA1UECgwHR2FicmllbDELMAkGA1UEBhMCQkUwHhcNMjAwMjI5MTUzOTA3WhcNMzAwMjI2MTUzOTA3WjBFMRUwEwYDVQQDDAxTU08gSWRQIFRlc3QxDTALBgNVBAsMBFRFU1QxEDAOBgNVBAoMB0dhYnJpZWwxCzAJBgNVBAYTAkJFMIIBIjANBgkqhkiG9w0BAQEFAAOCAQ8AMIIBCgKCAQEAxqT5LQIWpVL+PuC8IeKT6P6mUN4Si7wR8PP85NulKH0UMCz/6s51mBRln5O3PGVQ6hmyGMMII2D4Lu7NQkD58AsIN9Tlo59Qa5R5ox6oxGlGcv5E4YuxC3CZByfv7vvIx9m2TIyO6VIGYvmVeG6I21vgIEq/bd2jehuWcX6yPqe2c1T4aRje5fWciq3XrBEA7ff3oLAZcHyeyQLAGN5yZKEe1qED07ByFR4d8cJ64Hm4pXrXPCKW99YCRyH9u+9C05Jor4w/X2tj4UHHSzAOUnk4fy96z0VspVxdzeXUVAeUm06bj3Iz2o22lyo9Oye035+4A096mwvaMpPATVE1fwIDAQABo1AwTjAdBgNVHQ4EFgQUKrrRqAPLOlvDi9bRYuo3Y4Z0HMowHwYDVR0jBBgwFoAUKrrRqAPLOlvDi9bRYuo3Y4Z0HMowDAYDVR0TBAUwAwEB/zANBgkqhkiG9w0BAQsFAAOCAQEAtS2doXfkhmKCykKY6G7qi9hBYokaBUZ0tt7JQE20MseKKGzE5NCPe1pyu+jyhCeuunyliY4xfcO9LgdD9dUUw5fTa95DaNYzEbnppRAu9NySGU8RPH9GaGHkIk7s/8zEKRyCaLdn/jwWON62qR1oJgtXmdocJgeiaQvX49t5jrmbRA3PrB+SKMjFhqfOSntHHy/Z+xRudVSlIbvQPIy4uJE3IRwvuKY9pI+T6hlsQrmyN8s8WskPXwdVzF0sg3LvvH1UMMkNdjyGWCgYG0u618V1XrVUezgoC7wFj0ok4zpPSJahpFQoRcqFM0STd2IeYozLmmegXbJJxdDQlcVMlg==',
  'NameIDFormat' => 'urn:oasis:names:tc:SAML:2.0:nameid-format:transient',
  'contacts' => 
  array (
    0 => 
    array (
      'emailAddress' => 'gabriel@example.org',
      'contactType' => 'technical',
      'givenName' => 'Gabriel',
    ),
  ),
);

