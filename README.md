# Installation of the simplesamlphp

## Download docker-compose

```
curl -L "https://github.com/docker/compose/releases/download/1.25.4/docker-compose-$(uname -s)-$(uname -m)" -o docker-compose
mkdir ~/bin
chmod +x docker-compose
mv docker-compose ~/bin/
```

## Download simplesamlphp 

```
wget https://github.com/simplesamlphp/simplesamlphp/releases/download/v1.18.4/simplesamlphp-1.18.4.tar.gz
tar xzf simplesamlphp-1.18.4.tar.gz
mv simplesamlphp-1.18.4 simplesamlphp/simplesamlphp
rm simplesamlphp-1.18.4.tar.gz
```

## Build and start the docker image

```
sudo ~/bin/docker-compose build
sudo ~/bin/docker-compose up -d
```
